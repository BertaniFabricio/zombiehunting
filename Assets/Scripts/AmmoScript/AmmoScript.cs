﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class AmmoScript : MonoBehaviour
	{
		public AudioClip Bullets;

		private Transform _playerTrans = null;

		GunScript _playerGun;
		GameObject Gun;

		void Start()
		{
			_playerTrans = GameObject.FindGameObjectWithTag ("Player").transform;
			_playerGun = GameObject.FindGameObjectWithTag ("Gun").GetComponent<GunScript> ();
		}

		void Update()
		{
			if (Input.GetKeyDown (KeyCode.E))
			{
				if (Vector3.Distance (_playerTrans.position, this.transform.position) < 2.5f)
				{
					_playerGun.newMagazines ();
					Destroy (gameObject);
				}
			}
		}
	}
}