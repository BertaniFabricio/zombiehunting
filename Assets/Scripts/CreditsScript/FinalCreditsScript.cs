﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace ZombieHunting
{
	public class FinalCreditsScript : MonoBehaviour
	{
		void Start()
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			StartCoroutine (WaitCredits ());
		}

		public IEnumerator WaitCredits()
		{
			AsyncOperation async = SceneManager.LoadSceneAsync ("Menu", LoadSceneMode.Single);
			async.allowSceneActivation = false;
			yield return new WaitForSeconds (120.0f);
			async.allowSceneActivation = true;
		}
	}
}