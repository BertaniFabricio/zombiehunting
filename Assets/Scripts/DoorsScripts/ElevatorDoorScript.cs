﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class ElevatorDoorScript : MonoBehaviour
	{
		public GameObject PuertaPlantaBaja;
		public bool open = false;
		public AudioClip ElevatorDoorOpen;

		Animation otherAnimation;
		Elevator _boolsElevator;

		void Awake()
		{
			otherAnimation = PuertaPlantaBaja.GetComponent<Animation> ();
			_boolsElevator = GameObject.Find ("elevator_A").GetComponent<Elevator> ();
		}

		void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.tag == "Player")
			{
				if (!open && _boolsElevator.PlantaBaja)
				{
					otherAnimation.Play ("OpenDoor");
					GetComponent<AudioSource> ().clip = ElevatorDoorOpen;
					GetComponent<AudioSource> ().Play();
					open = true;
				}

			}
		}
	}
}