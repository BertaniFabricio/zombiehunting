﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class allDoors : MonoBehaviour
	{
		public float doorOpenAngle = 90.0f;
		public float doorCloseAngle = 0.0f;
		public float doorAnimSpeed = 2.0f;

		private Quaternion _doorOpen = Quaternion.identity;
		private Quaternion _doorClose = Quaternion.identity;

		private Transform _playerTrans = null;

		public bool doorStatus = false;
		private bool _doorGo = false;

		public AudioClip doorOpenSound;
		public AudioClip doorCloseSound;
		
		GameManager _gmScript;

		void Start()
		{
			doorStatus = false;
			
			_gmScript = GameObject.FindGameObjectWithTag ("GmGen").GetComponent<GameManager>();

			_doorOpen = Quaternion.Euler (0, doorOpenAngle, 0);
			_doorClose = Quaternion.Euler (0, doorCloseAngle, 0);
			
			if (_gmScript.isIntro == false)
			{
				_playerTrans = GameObject.FindGameObjectWithTag ("Player").transform;
			}
		}

		public IEnumerator moveDoor(Quaternion dest)
		{
			_doorGo = true;

			while (Quaternion.Angle (transform.localRotation, dest) > 4.0f)
			{
				transform.localRotation = Quaternion.Slerp (transform.localRotation, dest, Time.deltaTime * doorAnimSpeed);
				yield return null;
			}

			doorStatus = !doorStatus;
			_doorGo = false;

			yield return null;
		}

		void Update()
		{
			if (_gmScript.isIntro == false)
			{
				_playerTrans = GameObject.FindGameObjectWithTag ("Player").transform;
			}
			
			if (Input.GetKeyDown (KeyCode.E) && !_doorGo)
			{
				if (Vector3.Distance (_playerTrans.position, this.transform.position) < 2.5f)
				{
					if (doorStatus)
					{
						StartCoroutine (this.moveDoor (_doorClose));
						GetComponent<AudioSource> ().clip = doorCloseSound;
						GetComponent<AudioSource> ().Play ();
					}
					else
					{
						StartCoroutine (this.moveDoor (_doorOpen));
						GetComponent<AudioSource> ().clip = doorOpenSound;
						GetComponent<AudioSource> ().Play ();
					}		
				}
			}
		}
	}
}