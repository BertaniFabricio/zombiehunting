﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class ButtonDown1Script : Elevator
	{
		public IEnumerator BajarAPlantaBaja(){
			PuertaPA.Play ("CloseDoor");
			GetComponent<AudioSource> ().clip = ElevatorDoorClose;
			GetComponent<AudioSource> ().Play ();
			yield return new WaitForSeconds (2.0f);
			elevatorAnim.Play ("1DownToPB");
			GetComponent<AudioSource> ().clip = ElevatorSound;
			GetComponent<AudioSource> ().Play ();
			yield return new WaitForSeconds (5.30f);
			GetComponent<AudioSource> ().clip = ElevatorSound;
			GetComponent<AudioSource> ().Stop ();
			PuertaPB.Play ("OpenDoor");
			GetComponent<AudioSource> ().clip = ElevatorDoorOpen;
			GetComponent<AudioSource> ().Play();
		}

		public IEnumerator BajarAMorgue(){
			PuertaPB.Play ("CloseDoor");
			GetComponent<AudioSource> ().clip = ElevatorDoorClose;
			GetComponent<AudioSource> ().Play ();
			yield return new WaitForSeconds (2.0f);
			elevatorAnim.Play ("1DownToMorgue");
			GetComponent<AudioSource> ().clip = ElevatorSound;
			GetComponent<AudioSource> ().Play ();
			yield return new WaitForSeconds (7.0f);
			GetComponent<AudioSource> ().clip = ElevatorSound;
			GetComponent<AudioSource> ().Stop ();
			PuertaME.Play ("OpenDoor");
			GetComponent<AudioSource> ().clip = ElevatorDoorOpen;
			GetComponent<AudioSource> ().Play();
		}

		void OnTriggerEnter(Collider other){
			if ((PrimerPiso) && (other.gameObject.tag == "Player"))
			{
				StartCoroutine (BajarAPlantaBaja ());
			}

			if ((PlantaBaja) && (other.gameObject.tag == "Player"))
			{
				StartCoroutine (BajarAMorgue ());
			}
		}
	}
}