﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class ButtonUp1Script : Elevator
	{
		public IEnumerator SubirAlPisoUno()
		{
			PuertaPB.Play ("CloseDoor");
			GetComponent<AudioSource> ().clip = ElevatorDoorClose;
			GetComponent<AudioSource> ().Play ();
			yield return new WaitForSeconds (2.0f);
			elevatorAnim.Play ("1UpToPA");
			GetComponent<AudioSource> ().clip = ElevatorSound;
			GetComponent<AudioSource> ().Play ();
			yield return new WaitForSeconds (5.30f);
			GetComponent<AudioSource> ().clip = ElevatorSound;
			GetComponent<AudioSource> ().Stop ();
			PuertaPA.Play ("OpenDoor");
			GetComponent<AudioSource> ().clip = ElevatorDoorOpen;
			GetComponent<AudioSource> ().Play();
		}

		public IEnumerator SubirAPlantaBaja()
		{
			PuertaME.Play ("CloseDoor");
			GetComponent<AudioSource> ().clip = ElevatorDoorClose;
			GetComponent<AudioSource> ().Play ();
			yield return new WaitForSeconds (2.0f);
			elevatorAnim.Play ("1UpToPB");
			GetComponent<AudioSource> ().clip = ElevatorSound;
			GetComponent<AudioSource> ().Play ();
			yield return new WaitForSeconds (7.0f);
			GetComponent<AudioSource> ().clip = ElevatorSound;
			GetComponent<AudioSource> ().Stop ();
			PuertaPB.Play ("OpenDoor");
			GetComponent<AudioSource> ().clip = ElevatorDoorOpen;
			GetComponent<AudioSource> ().Play();
		}

		void OnTriggerEnter(Collider other)
		{
			if ((PlantaBaja) && (other.gameObject.tag == "Player"))
			{
				StartCoroutine (SubirAlPisoUno ());
			}

			if ((Morgue) && (other.gameObject.tag == "Player"))
			{
				StartCoroutine (SubirAPlantaBaja ());
			}
		}
	}
}