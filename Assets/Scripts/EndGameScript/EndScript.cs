﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace ZombieHunting
{
	public class EndScript : MonoBehaviour
	{
		public Text score;

		GameManagerCounters _gmCounters;

		void Start()
		{
			_gmCounters = GameObject.FindGameObjectWithTag ("gmcounter").GetComponent<GameManagerCounters> ();
		}

		void drawScore()
		{
			score.text = "Tu puntaje fue: " + _gmCounters.scoreValue.ToString ();
		}

		void Update()
		{
			drawScore ();
		}
	}
}