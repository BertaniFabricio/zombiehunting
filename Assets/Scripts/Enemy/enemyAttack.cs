﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class enemyAttack : enemyScript
	{
		public int infligedDamage;

		private float _timeBetweenAttacks = 1.0f;
		private float _timer;
		private bool _playerInRange;

		PlayerScript _playerScript;
		enemyScript _enemyScript;
		Animator _anim;

		void Awake()
		{
			_enemyScript = GetComponent<enemyScript> ();
			_anim = GetComponent<Animator> ();
			_playerScript = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerScript> ();
			_agent = GetComponent<NavMeshAgent> ();
		}

		void OnTriggerEnter (Collider other)
		{
			if (other.gameObject.tag == "Player")
			{
				_playerInRange = true;
				_agent.Stop ();
				_anim.SetBool ("isPlayer", true);
			}
		}

		void OnTriggerExit (Collider other)
		{
			if (other.gameObject.tag == "Player")
			{
				_playerInRange = false;
				_agent.Resume ();
				_anim.SetBool ("isPlayer", false);
			}
		}

		void Attack()
		{
			_timer = 0.0f;

			if (_playerScript.health > 0)
			{
				_playerScript.takeDamage (infligedDamage);
				_playerScript.damaged = true;
			}
		}

		void Update()
		{
			_timer += Time.deltaTime;

			if (_timer >= _timeBetweenAttacks && _playerInRange && _enemyScript.health > 0) {
				Attack ();
			}
			else
			{
				_playerScript.damaged = false;
			}
		}
	}
}