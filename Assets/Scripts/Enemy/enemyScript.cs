﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace ZombieHunting
{
	public class enemyScript : MonoBehaviour
	{
		public int damageAmount;
		public int health;
		public GameObject slider;
		public Slider enemySlider;
		 
		protected int _startingHealth = 100;
		protected NavMeshAgent _agent;
		protected Animator _animator;
		protected float deathTime;

		Transform _player;
		protected GameManagerCounters _gmcounter;

		void Awake()
		{
			_animator = GetComponent<Animator> ();
			_agent = GetComponent<NavMeshAgent> ();
		}

		void Start()
		{
			_gmcounter = GameObject.FindGameObjectWithTag ("gmcounter").GetComponent<GameManagerCounters> ();
			GameObject playerGameobject = GameObject.FindGameObjectWithTag ("Player");
			health = _startingHealth;
			_player = playerGameobject.transform;
			slider.SetActive (true);
		}

		void Walk()
		{
			if (health > 0)
			{
				_agent.destination = _player.transform.position;
			}
		}

		public void takeDamage()
		{
			health -= damageAmount;
			enemySlider.value = health;
		}

		void isDead()
		{
			if (health <= 0)
			{
				slider.SetActive (false);
				_animator.SetBool ("isDead", true);
				_agent.Stop ();

				startDeathTime ();
			}
		}

		void startDeathTime()
		{
			deathTime += Time.deltaTime;
			if (deathTime >= 2.0f)
			{
				_gmcounter.enemyDead ();
				Destroy (gameObject);
			}
		}

		void Update()
		{
			Walk ();
			isDead ();
		}
	}
}