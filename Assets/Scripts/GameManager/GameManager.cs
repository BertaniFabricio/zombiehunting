﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class GameManager : MonoBehaviour
	{
		public GameObject Player;
		public GameObject Hud;
		public GameObject Objetives;
		public GameObject Pause;
		public GameObject GameOver;
		public GameObject EndGame;
		public GameObject ExitTrigger;
		public GameObject Medicines;
		public GameObject Ammunitions;
		public GameObject Goal;
		public GameObject Tutorial;
		public GameObject Enemies;
		public GameObject Triggers;
		public GameObject SoundHA;

		public GameObject Intro;

		public GameObject GMCounter;
		public GameObject GMPause;
		public GameObject GMObjetives;
		public GameObject GMGameOver;
		
		public bool isIntro;

		private bool _escPressed;

		void Start()
		{
			Player.SetActive (false);
			Hud.SetActive (false);
			Objetives.SetActive (false);
			Pause.SetActive (false);
			GameOver.SetActive (false);
			EndGame.SetActive (false);
			ExitTrigger.SetActive (false);
			Medicines.SetActive (false);
			Ammunitions.SetActive (false);
			Goal.SetActive (false);
			Tutorial.SetActive (false);
			Enemies.SetActive (false);
			Triggers.SetActive (false);
			SoundHA.SetActive (false);
			Intro.SetActive (false);

			GMCounter.SetActive (false);
			GMPause.SetActive (false);
			GMObjetives.SetActive (false);
			GMGameOver.SetActive (false);

			_escPressed = false;

			StartCoroutine (generateIntro ());
		}

		IEnumerator generateIntro()
		{
			isIntro = true;
			Intro.SetActive (true);
			yield return new WaitForSeconds (167.31f);
			StartCoroutine (generateGame ());

			yield return null;
		}

		IEnumerator generateGame()
		{
			Destroy (Intro);
			isIntro = false;
			Player.SetActive (true);
			Hud.SetActive (true);
			Medicines.SetActive (true);
			Ammunitions.SetActive (true);
			Goal.SetActive (true);
			Tutorial.SetActive (true);
			Enemies.SetActive (true);
			Triggers.SetActive (true);
			SoundHA.SetActive (true);

			GMCounter.SetActive (true);
			GMPause.SetActive (true);
			GMObjetives.SetActive (true);
			GMGameOver.SetActive (true);

			yield return null;
		}

		void Update()
		{
			if (Input.GetKeyDown (KeyCode.Escape))
			{
				_escPressed = true;
				if (_escPressed)
				{
					StopCoroutine (generateIntro ());
					_escPressed = false;
					StartCoroutine (generateGame ());
				}
			}
		}
	}
}