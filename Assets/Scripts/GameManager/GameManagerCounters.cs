﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class GameManagerCounters : MonoBehaviour
	{
		public int enemiesCount;
		public int scoreValue;
		public int keysValue;

		public void enemyDead()
		{
			enemiesCount++;
			scoreValue = scoreValue + 10;
		}

		public void KeyFound()
		{
			keysValue++;
		}
	}
}