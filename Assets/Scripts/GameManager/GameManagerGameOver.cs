﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

namespace ZombieHunting
{
	public class GameManagerGameOver : MonoBehaviour
	{
		public GameObject GameOverDisplay;

		FirstPersonController _fps;
		GunScript _gunScript;
		PlayerScript _playerScript;

		void Start()
		{
			_fps = GameObject.FindGameObjectWithTag ("Player").GetComponent<FirstPersonController> ();
			_playerScript = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerScript> ();
			_gunScript = GameObject.FindGameObjectWithTag ("Gun").GetComponent<GunScript> ();
			GameOverDisplay.SetActive (false);
		}

		IEnumerator timeToRestart()
		{
			yield return new WaitForSeconds (8.0f);
			SceneManager.LoadScene ("Hospital");

			yield return null;
		}

		void Update()
		{
			if (_playerScript.health <= 0)
			{
				GameOverDisplay.SetActive (true);
				_fps.enabled = false;
				_gunScript.enabled = false;
				StartCoroutine (timeToRestart ());
			}
		}
	}
}