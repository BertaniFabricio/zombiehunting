﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class GameManagerObjetives : MonoBehaviour
	{
		public GameObject objetivesHud;
		public GameObject obj1;
		public GameObject obj2;
		public GameObject obj3;
		public GameObject strike1;
		public GameObject strike2;
		public GameObject exitTrigger;

		private bool obj1Accomplished = false;
		private bool obj2Accomplished = false;

		GameManagerCounters _gmCounter;


		void Start()
		{
			_gmCounter = GameObject.FindGameObjectWithTag ("gmcounter").GetComponent<GameManagerCounters> ();
			objetivesHud.SetActive (false);
			obj3.SetActive (false);
			strike1.SetActive (false);
			strike2.SetActive (false);
			exitTrigger.SetActive (false);
		}

		void objetivesUpdated()
		{
			if (_gmCounter.enemiesCount == 28)
			{
				obj1Accomplished = true;
				strike1.SetActive (true);
			}
			if (_gmCounter.keysValue == 3)
			{
				obj2Accomplished = true;
				strike2.SetActive (true);
			}

			if (obj1Accomplished && obj2Accomplished)
			{
				obj3.SetActive (true);
				exitTrigger.SetActive (true);
			}
		}

		void Update()
		{
			if (Input.GetKeyDown (KeyCode.Tab))
			{
				objetivesHud.SetActive (true);
			}

			if (Input.GetKeyUp (KeyCode.Tab))
			{
				objetivesHud.SetActive (false);
			}

			objetivesUpdated ();
		}
	}
}