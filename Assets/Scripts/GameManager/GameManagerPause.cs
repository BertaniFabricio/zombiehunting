﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

namespace ZombieHunting
{
	public class GameManagerPause : MonoBehaviour
	{
		public GameObject hudDisplay;
		public GameObject pauseDisplay;

		private bool isPaused = false;

		FirstPersonController _fps;
		GunScript _gunScript;
		hudScript _hudScript;


		void Start()
		{
			Time.timeScale = 1;
			_fps = GameObject.FindGameObjectWithTag ("Player").GetComponent<FirstPersonController> ();
			_gunScript = GameObject.FindGameObjectWithTag ("Gun").GetComponent<GunScript> ();
			_hudScript = GameObject.FindGameObjectWithTag ("hud").GetComponent<hudScript> ();
			pauseDisplay.SetActive (false);
		}

		public void onClickReturn()
		{
			Time.timeScale = 1;
			_fps.enabled = true;
			_gunScript.enabled = true;
			_hudScript.enabled = true;
			pauseDisplay.SetActive (false);
			hudDisplay.SetActive (true);
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = false;
		}

		public void onClickExit()
		{
			Time.timeScale = 1;
			SceneManager.LoadScene ("Menu");
		}

		void Update()
		{
			if (Input.GetKeyDown (KeyCode.Escape))
			{
				isPaused = !isPaused;
				if (isPaused)
				{
					Time.timeScale = 0;
					_gunScript.enabled = false;
					_fps.enabled = false;
					_hudScript.enabled = false;
					hudDisplay.SetActive (false);
					pauseDisplay.SetActive (true);
					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = true;
				}
				else if (!isPaused)
				{
					Time.timeScale = 1;
					_fps.enabled = true;
					_gunScript.enabled = true;
					_hudScript.enabled = true;
					pauseDisplay.SetActive (false);
					hudDisplay.SetActive (true);
					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = false;
				}
			}
		}
	}
}