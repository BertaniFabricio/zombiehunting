﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace ZombieHunting
{
	public class GameOverScript : MonoBehaviour
	{
		public Text score;

		private Animation _anim;

		PlayerScript _playerScript;
		GameManagerCounters _gmCounters;

		void Awake()
		{
			_anim = GetComponent<Animation> ();
		}

		void Start()
		{
			_playerScript = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerScript> ();
			_gmCounters = GameObject.FindGameObjectWithTag ("gmcounter").GetComponent<GameManagerCounters> ();
		}

		void PlayAnim()
		{
			if (_playerScript.isDead)
			{
				_anim.Play ("GameOver");
			}
		}

		void drawScore()
		{
			score.text = "Tu puntaje fue: " + _gmCounters.scoreValue.ToString ();
		}

		void Update()
		{
			drawScore ();
		}
	}
}