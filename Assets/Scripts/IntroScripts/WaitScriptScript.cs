﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class WaitScriptScript : MonoBehaviour
	{
		public GameObject Derrick;
		public GameObject Derrick1;
		public GameObject Corpse;

		private Animator _animator;

		void Awake()
		{
			_animator = GetComponent<Animator> ();
		}

		void Start ()
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			_animator.enabled = true;
		}
	}
}