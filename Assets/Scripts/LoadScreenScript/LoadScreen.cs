﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace ZombieHunting
{
	public class LoadScreen : MonoBehaviour
	{
		void Start ()
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			StartCoroutine (WaitLoad ());	
		}

		public IEnumerator WaitLoad()
		{
			AsyncOperation async = SceneManager.LoadSceneAsync ("Hospital", LoadSceneMode.Single);
			async.allowSceneActivation = false;
			yield return new WaitForSeconds (15.0f);
			async.allowSceneActivation = true;
		}
	}
}