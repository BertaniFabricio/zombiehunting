﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class MedicineScript : MonoBehaviour
	{
		public AudioClip clip;

		private Transform _playerTrans = null;	

		PlayerScript _playerScript;

		void Start ()
		{
			_playerTrans = GameObject.FindGameObjectWithTag ("Player").transform;
			_playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript> ();
		}

		void Update()
		{
			if (Input.GetKeyDown (KeyCode.E))
			{
				if (Vector3.Distance (_playerTrans.position, this.transform.position) < 2.5f)
				{
					if (_playerScript.health < 100)
					{
						_playerScript.Medicine ();
						Destroy (gameObject);
					}
				}
			}
		}
	}
}