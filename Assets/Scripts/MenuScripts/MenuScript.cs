﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class MenuScript : MonoBehaviour 
	{
		public GameObject MainCamera;

		public AudioClip MainTheme;

		Animator _menuAnimation;

		void Awake()
		{
			_menuAnimation = MainCamera.GetComponent<Animator> ();
		}

		public void OnClickExit()
		{
			_menuAnimation.SetInteger ("isExit", 1);
			_menuAnimation.SetInteger ("isNoExit", 0);
		}

		public void NoClickExit()
		{
			_menuAnimation.SetInteger ("isNoExit", 1);
			_menuAnimation.SetInteger ("isExit", 0);
		}

		public void OnClickQuit()
		{
			Application.Quit ();
		}

		public void OnClickPlay()
		{
			_menuAnimation.SetInteger ("isPlay", 1);
		}

		public void OnClickControl()
		{
			_menuAnimation.SetInteger ("isControl", 1);
			_menuAnimation.SetInteger ("ControlOut", 0);
		}

		public void OnClickControlOut()
		{
			_menuAnimation.SetInteger ("ControlOut", 1);
			_menuAnimation.SetInteger ("isControl", 0);
		}

		void Start ()
		{
			GetComponent<AudioSource> ().clip = MainTheme;
			GetComponent<AudioSource> ().Play ();
		}
	}
}