using UnityEngine;
using System.Collections;
 
 namespace ZombieHunting
{
	 [RequireComponent (typeof(Rigidbody))]
	 public class Movement : MonoBehaviour
	{
		public float Smooth = 1f;

		private Quaternion _targetRotation;

		void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.tag == "Spawn1")
			{			
				_targetRotation = Quaternion.LookRotation (-transform.forward, Vector3.up);
				transform.rotation = Quaternion.Slerp (transform.rotation, _targetRotation, Smooth * Time.deltaTime);
			}

			if (other.gameObject.tag == "Spawn2")
			{
				_targetRotation = Quaternion.LookRotation (-transform.forward, Vector3.up);
				transform.rotation = Quaternion.Slerp (transform.rotation, _targetRotation, Smooth * Time.deltaTime);
			}
		}

		void Update()
		{
			transform.Translate(Vector3.forward * Time.deltaTime);
		}
	}
}