﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace ZombieHunting
{
	public class PlayGameScript : MonoBehaviour
	{
		void Start ()
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}

		void OnTriggerEnter(Collider Other)
		{
			if (Other.gameObject.tag == "PlayTrigger")
			{
				SceneManager.LoadScene ("LoadScreen");
			}
		}
	}
}