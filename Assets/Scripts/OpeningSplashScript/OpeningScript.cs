﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace ZombieHunting
{
	public class OpeningScript : MonoBehaviour
	{
		void Start ()
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			StartCoroutine (WaitOpening ());	
		}

		public IEnumerator WaitOpening()
		{
			AsyncOperation async = SceneManager.LoadSceneAsync ("Menu", LoadSceneMode.Single);
			async.allowSceneActivation = false;
			yield return new WaitForSeconds (15.0f);
			async.allowSceneActivation = true;
		}
	}
}