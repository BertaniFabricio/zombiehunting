﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class FlashLightScript : MonoBehaviour
	{
		public AudioClip flashLightOn;
		public AudioClip flashLightOff;
		private Light flashLight;
		private bool On = false;

		void Awake()
		{
			flashLight = GetComponent<Light> ();
		}

		void Update ()
		{
			if (Input.GetKeyDown (KeyCode.F))
			{
				On = !On;
				if (On) {
					flashLight.enabled = true;
					GetComponent<AudioSource> ().clip = flashLightOn;
					GetComponent<AudioSource> ().Play ();
				}
				else if (!On)
				{
					flashLight.enabled = false;
					GetComponent<AudioSource> ().clip = flashLightOff;
					GetComponent<AudioSource> ().Play ();
				}
					
			}	
		}
	}
}