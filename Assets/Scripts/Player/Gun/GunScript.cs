using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace ZombieHunting
{
	public class GunScript : MonoBehaviour
	{
		public GameObject FPS_WeaponFire;
		public GameObject bulletHole;
		public GameObject muzzle1;
		public GameObject muzzle2;

		public AudioClip shotSound;
		public AudioClip emptySound;
		public AudioClip reloadSound;

		public int bulletsPerMagazine; //Para el hud y el disparo
		public int magazinesRemaining; //Para el hud y el disparo

		private Ray ray;
		private RaycastHit hit;
		private Animator anim;
		private float _reloadTime = 1.25f;
		private float _shotTime = 0.15f;
		private float _nextShot;
		private float _nextReload;
		private int _ammoAmount = 9;
		private int _intShoot = Animator.StringToHash ("ShootOnce");
		private int _intReload = Animator.StringToHash ("ReloadOnce");

		void Awake()
		{
			anim = FPS_WeaponFire.GetComponent<Animator> ();
		}

		void Start()
		{
			bulletsPerMagazine = _ammoAmount;
			magazinesRemaining = 3;
			_nextShot = Time.time;
			_nextReload = Time.time;
		}

		void shootRaycast()
		{
			Vector2 screenCenterPoint = new Vector2 (Screen.width / 2, Screen.height / 2);

			ray = Camera.main.ScreenPointToRay (screenCenterPoint);

			if (Physics.Raycast (ray, out hit, Camera.main.farClipPlane))
			{
				Vector3 pos = hit.point;
				Quaternion rot = Quaternion.FromToRotation (Vector3.up, hit.normal);

				if (hit.collider.gameObject.tag == "wood")
				{
					hit.collider.gameObject.GetComponent<hitWood> ().rayHitWood (hit);
				}

				if (hit.collider.gameObject.tag == "concrete")
				{
					hit.collider.gameObject.GetComponent<hitConcrete> ().rayHitConcrete (hit);
				}

				if (hit.collider.gameObject.tag == "metal")
				{
					hit.collider.gameObject.GetComponent<hitMetal> ().rayHitMetal (hit);
				}

				if (hit.collider.gameObject.tag == "dirt")
				{
					hit.collider.gameObject.GetComponent<hitDirt> ().rayHitDirty (hit);
				}

				if (hit.collider.gameObject.tag == "flesh")
				{
					hit.collider.gameObject.GetComponent<hitFlesh> ().rayHitFlesh (hit);
				}

				if (hit.collider.gameObject.tag == "Enemy")
				{
					hit.collider.gameObject.GetComponent<hitEnemy> ().rayHitEnemy (hit);
				}
				
				if (hit.collider.gameObject.tag != "Enemy")
				{
					GameObject hole;
					hole = Instantiate (bulletHole, pos, rot) as GameObject;
					hole.transform.parent = hit.collider.transform;
				}
			}
		}

		void Shooting()
		{
			if (_nextShot <= Time.time)
			{
				_nextShot = Time.time + _shotTime;
				if (bulletsPerMagazine > 0) {
					GetComponent<AudioSource> ().clip = shotSound;
					GetComponent<AudioSource> ().Play ();
					shootRaycast ();
					anim.SetTrigger (_intShoot);
					bulletsPerMagazine--;
					muzzle1.SetActive (true);
					muzzle2.SetActive (true);
				}
				else
				{
					GetComponent<AudioSource> ().clip = emptySound;
					GetComponent<AudioSource> ().Play ();
				}
			}
		}

		void Reload()
		{
			if (_nextReload <= Time.time)
			{
				if (magazinesRemaining > 0)
				{
					if (bulletsPerMagazine <= 0)
					{
						GetComponent<AudioSource> ().clip = reloadSound;
						GetComponent<AudioSource> ().Play ();
						anim.SetTrigger (_intReload);
						bulletsPerMagazine += _ammoAmount;
						magazinesRemaining--;
						_nextReload = Time.time + _reloadTime;
						_nextShot = _nextReload;
					}
				}
			}
		}

		public void newMagazines()
		{
			magazinesRemaining = magazinesRemaining + 3;
		}

		void Update()
		{
			if (Input.GetButtonDown("Fire1"))
			{
				Shooting ();
			}

			if (Input.GetKeyDown (KeyCode.R))
			{
				Reload ();
			}
		}
	}
}