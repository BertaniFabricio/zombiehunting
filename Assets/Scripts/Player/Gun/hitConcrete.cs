﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class hitConcrete : MonoBehaviour
	{
		public GameObject concreteParticle;

		public void rayHitConcrete (RaycastHit hit)
		{
			Vector3 pos = hit.point;
			Quaternion rot = Quaternion.FromToRotation (Vector3.up, hit.normal);
			GameObject Particle = Instantiate (concreteParticle, pos, rot) as GameObject;
			Particle.transform.parent = this.transform;
		}
	}
}