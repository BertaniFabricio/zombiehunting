﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class hitEnemy : enemyScript
	{
		public GameObject bloodParticle;

		public void rayHitEnemy(RaycastHit hit)
		{
			Vector3 pos = hit.point;
			Quaternion rot = Quaternion.FromToRotation (Vector3.up, hit.normal);
			GameObject Particle = Instantiate (bloodParticle, pos, rot) as GameObject;
			Particle.transform.parent = this.transform;
			takeDamage ();
		}
	}
}