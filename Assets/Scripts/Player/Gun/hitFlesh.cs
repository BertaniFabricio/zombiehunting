﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class hitFlesh : MonoBehaviour
	{
		public GameObject bloodParticle;

		public void rayHitFlesh (RaycastHit hit)
		{
			Vector3 pos = hit.point;
			Quaternion rot = Quaternion.FromToRotation (Vector3.up, hit.normal);
			GameObject Particle = Instantiate (bloodParticle, pos, rot) as GameObject;
			Particle.transform.parent = this.transform;
		}
	}
}