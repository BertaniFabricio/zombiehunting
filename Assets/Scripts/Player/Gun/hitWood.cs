﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class hitWood : MonoBehaviour
	{
		public GameObject woodParticle;

		public void rayHitWood(RaycastHit hit)
		{
			Vector3 pos = hit.point;
			Quaternion rot = Quaternion.FromToRotation (Vector3.up, hit.normal);
			GameObject Particle = Instantiate (woodParticle, pos, rot) as GameObject;
			Particle.transform.parent = this.transform;
		}
	}
}