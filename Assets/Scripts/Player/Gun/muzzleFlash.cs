﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class muzzleFlash : MonoBehaviour
	{
		public float lifeTime;

		void Start()
		{
			
		}

		void OnEnable()
		{
			Invoke ("setAct", lifeTime);
		}

		void setAct()
		{
			gameObject.SetActive (false);
		}

		void Update()
		{
			transform.localEulerAngles = new Vector3 (Random.Range (0, 359), 0, 0);
		}
	}
}