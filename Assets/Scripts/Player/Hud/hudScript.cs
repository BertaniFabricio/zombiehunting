﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace ZombieHunting
{
	public class hudScript : MonoBehaviour
	{
		public Text bulletAmount;
		public Text magazinesAmount;
		public Text healthAmount;
		public Text scoreAmount;
		public Text keysAmount;
		public Slider healthSlider;
		public Image damageEffect;

		GunScript _gunScript;
		PlayerScript _playerScript;
		GameManagerCounters _gmCounters;

		void Start()
		{
			_gunScript = GameObject.FindGameObjectWithTag ("Gun").GetComponent<GunScript> ();
			_playerScript = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerScript> ();
			_gmCounters = GameObject.FindGameObjectWithTag ("gmcounter").GetComponent<GameManagerCounters> ();
			damageEffect.enabled = false;
		}

		void drawHealthBar()
		{
			healthAmount.text = _playerScript.health.ToString ();
			healthSlider.value = _playerScript.health;
			if (_playerScript.damaged) {
				damageEffect.enabled = true;
			}
			else
			{
				damageEffect.enabled = false;
			}
		}

		void drawAmmunition()
		{
			bulletAmount.text = _gunScript.bulletsPerMagazine.ToString ();
			magazinesAmount.text = _gunScript.magazinesRemaining.ToString ();
		}

		void drawScore()
		{
			scoreAmount.text = "Score: " + _gmCounters.scoreValue.ToString ();
		}

		void drawKeys()
		{
			keysAmount.text = _gmCounters.keysValue.ToString ();
		}

		void Update()
		{
			drawAmmunition ();
			drawHealthBar ();
			drawScore ();
			drawKeys ();
		}
	}
}