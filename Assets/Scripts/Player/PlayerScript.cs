using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

namespace ZombieHunting
{
	public class PlayerScript : MonoBehaviour
	{
		private int _startingHealth = 100;

		public int health;

		public AudioClip hurtSound;
		public AudioClip deathSound;

		public bool isDead;
		public bool damaged;

		void Awake()
		{
			health = _startingHealth;
		}

		void Start()
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = false;
		}

		public void takeDamage (int amount)
		{
			//damaged = true;
			GetComponent<AudioSource> ().clip = hurtSound;
			GetComponent<AudioSource> ().Play ();

			health -= amount;
		}

		public void Dead()
		{
			if (health <= 0 && !isDead)
			{
				isDead = true;

				GetComponent<AudioSource> ().clip = deathSound;
				GetComponent<AudioSource> ().Play ();
			}
		}

		public void Medicine()
		{
			health = health + 15;
		}

		void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.tag == "Elevator")
			{
				this.transform.SetParent (other.transform, true);
			}
		}

		void OnTriggerExit(Collider other)
		{
			if (other.gameObject.tag == "Elevator")
			{
				this.transform.SetParent (null);
			}
		}

		void Update()
		{
			Dead ();
		}
	}
}